var express = require('express');
var app = express();
fs = require('fs');

function read_directory(path, next) {
  fs.readdir(path, function (err, files) {
    var count = files.length,
        results = {};
    next(files);
  });
}

var projects = null;
read_directory('./projects', function(data){
  projects = data;
});


app.use("/css", express.static(__dirname + '/css'));
app.use("/js", express.static(__dirname + '/js'));
app.use("/projects", express.static(__dirname + '/projects'));

app.set('view engine', 'ejs');

app.listen(8000);
console.log('Listening on port 8000');

app.get('/', function(req, res){
  res.render(
    'index',
    {
        'locals':{
            'projects': projects,
        }
    }
  );
});
