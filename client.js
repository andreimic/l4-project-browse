function getUrlVars(posid){
    var vars = [], hash;
    var hashes = posid.slice(posid.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
2
$("#browseTableBody tr")
    .each(function(i){
        
        var row = $(this);
        var td1 = $(row).children("td").get(0);
        var td2 = $(row).children("td").get(1);

        var title = $(td1).children("a").text();
        var url = $(td1).children("a").attr("href");
        var id = getUrlVars(url)["posid"];
        var superv = $(td2).children("a").text();

        var description = (function(){
            var d = null;
            $.ajax({
                'async': false,
                'global': false,
                'url': url,
                'success': function(data){ d = $(data).children("#position_info").html().trim(); },
                'error': function(data){ d = "ERROR"; }
            });
            return d;
        })();

        var proj = {
            title: title,
            id: id,
            superv: superv,
            description: description
        }

        console.log(proj);
        
        $.post(
            "http://localhost:8080",
            proj
        );

    });
