var sys = require("sys"),  
my_http = require("http");  
my_http.createServer(function(req,res){  
    if (req.method == 'POST') {
        console.log("POST");
        var body = '';
        
        req.on('data', function (data) {
            body += data;
            console.log("Partial body: " + body);
        });
        
        req.on('end', function () {
            var params = body.split('&') ;
            var project = {};

             for ( param in params ){
               var pair = params[param].split('=') ;
               // project[pair[0]] = pair[1];
               project[pair[0]] = decodeURIComponent(pair[1]).replace(/\+/g, " ");
               // console.log("Name: " + pair[0] + " = " + decodeURIComponent(pair[1]) + "\n") ;
             }
             console.log(project);

                var fs = require('fs');
                fs.writeFile("projects/" + project.id + ".json", JSON.stringify(project), function(err) {
                     if(err) {
                         console.log(err);
                     } else {
                         console.log("The file was saved!");
                     }
                 }); 

            res.end() ;
        });

        // res.writeHead(200, {'Content-Type': 'text/html'});
        // res.end('post received');
    }
    else if(req.method == 'GET'){
        res.writeHeader(200, {"Content-Type": "text/plain"});  
        res.write("Hello World");  
        res.end();  
    }

    // request.on('data', function (chunk) {

	   // var id = chunk.id;

    // /*	var fs = require('fs');
    // 	fs.writeFile("/tmp/test", "Hey there!", function(err) {
    // 	    if(err) {
    // 	        console.log(err);
    // 	    } else {
    // 	        console.log("The file was saved!");
    // 	    }
    // 	}); 
    // */
    // 	sys.puts("XXX: " + id);

    // });

}).listen(8080);  
sys.puts("Server Running on 8080");  
